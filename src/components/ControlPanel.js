import React, {Component} from 'react';

class ControlPanel extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      showAns: false
    }
  }

  handleChange (event) {

  }

  renderAnswer(){
    if(this.state.showAns){
      const digits = [];
      for(let id in this.props.guessResult){
        digits.push(<div key={id} className={"bigChar"}>{this.props.guessResult[id]}</div>)
      }
      return digits;
    }
  }

  handleShowAnswer3s(){
    this.setState({
      showAns:true
    },() =>{
      setTimeout( () => {
        this.setState({
          showAns: false
        })
      },3000)
    })
    console.log("should show")
  }

  handleShowAnswer = () =>{
    this.setState({
      showAns:true
    },() => {
      this.props.handleChangeState("newGame");
    });

  }
  render() {
    return(
      <div className={"controlPanel"}>
        <h1>New Card</h1>
        <button className={"startGameBtn"} onClick={() => { this.props.handleStartGame(); this.handleShowAnswer3s();}}>
          New Card
        </button><br/>
        {this.renderAnswer()}<br />
        <button className={"showAnswerBtn"} onClick={this.handleShowAnswer}>Show Answer</button>
      </div>
    )
  }
}

export default ControlPanel;