import React, {Component} from 'react';
import '../App.less';

class PlayingPanel extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      guessString: ""
    }
  }

  handleChange = (event) =>{
    this.setState({
      guessString: event.target.value
    },()=>{
      console.log(this.state.guessString)
    })
  };


  renderSuccess() {
    switch (this.props.state) {
      case 'waiting':
        return <div className={"hint"}> </div>;
      case 'success':
        return <div className={"hint-success"}> SUCCESS</div>;
      case 'failed':
        return <div className={"hint-failed"}> FAILED</div>;
    }
  }

  render4digits(){
    let guessString = this.state.guessString.padEnd(4);
    const digits = [];
    for(let id in guessString){
      digits.push(<div key={id} className={"bigChar"}>{guessString[id]}</div>)
    }
    return digits;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(this.props.state === "newGame"){
      this.setState({
        guessString: ""
      },() =>{
        this.props.handleChangeState({result:"waiting"});
      })
    }
  }


  render(){
    return(
      <div className={"playingPanel"}>
        <div className={"answerPanel"}>
          <h1>Your Result</h1>
          {this.render4digits()}
          {this.renderSuccess()}
        </div>
        <div className={"inputPanel"}>
          <h1>Guess Card </h1>
          <input type={"text"} maxLength = {4} onChange={this.handleChange} value={this.state.guessString}/>
          <button className={"guessBtn"} onClick = {this.props.handleGuessInput(this.state.guessString)}>Guess</button>
        </div>

      </div>
    )
  }
}

export default PlayingPanel;