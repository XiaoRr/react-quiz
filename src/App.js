import React, {Component} from 'react';
import './App.less';
import ControlPanel from "./components/ControlPanel";
import PlayingPanel from "./components/PlayingPanel";

class App extends Component{

  constructor(props) {
    super(props);
    this.state = {
      guessResult : "ABCD",
      result : "waiting"
    }
  }

  startGameHandler = () => {
    return (guessString) =>{

    }
  };

  handleStartGame = () => {
    let result = [];
    for(let i=0;i<4;i++){
      let ranNum = Math.ceil(Math.random() * 25);
      result.push(String.fromCharCode(65+ranNum));
    }
    let ans = result.join('');

    console.log("answer:",ans);
      this.setState({
        guessResult : ans,
        result : "newGame"
      })
  }

  handleGuessInput = (guessString) => {
    return () => {
      if(guessString === this.state.guessResult){
        console.log("SUCCESS")
        this.setState({
          result : "success"
        })
      }else{
        console.log("FAILED",this.state.guessResult,guessString);
        this.setState({
          result : "failed"
        })
      }
    }
  };

  handleChangeState = (result) =>{
    this.setState({
      result: result
    })
  }

  render() {
    return (
      <div className = 'App' >
        <ControlPanel handleStartGame = {this.handleStartGame}
                      guessResult = {this.state.guessResult}
                      handleChangeState = {this.handleChangeState}/>
        <PlayingPanel handleGuessInput = {this.handleGuessInput}
                      state={this.state.result}
                      handleChangeState = {this.handleChangeState}/>
      </div>
    );
  }x
}

// <image className="win" src="" />
export default App;